import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import axiosRetry from 'axios-retry';
import './App.css';
import GPTLogo from './assets/chatgpt.svg'
import addBtn from './assets/add-30.png'
import msgIcon from './assets/message.svg'
import home from './assets/home.svg'
import saved from './assets/bookmark.svg'
import rocket from './assets/rocket.svg'
import sendBtn from './assets/send.svg'
import userIcon from './assets/user-icon.png'
import GPTImgLogo from './assets/chatgptLogo.svg'

function App() {
  const msgEnd = useRef(null);
  const [input, setInput] = useState('');
  const [dialogID, setDialogID] = useState(1);
  const [dialogs, setDialogs] = useState([
    { dialogID: 1, messages: [] }
  ]);
  const [messages, setMessages] = useState([]);
  const IP_ADDRESS = process.env.REACT_APP_SERVER_IP

  useEffect(() => {
    msgEnd.current.scrollIntoView();
  }, [messages]);

  useEffect(() => {
    // TODO:
    axios.get(`http://${IP_ADDRESS}/api/message/allDialogs/`, {
      params: { "user_id": 1 }
    })
      .then((res) => {
        const response_dialogs = res.data.dialogs
        console.log(response_dialogs)
        setDialogs(response_dialogs)
        // console.log(dialogs)
        const messages = response_dialogs[0].messages
        // console.log(messages)
        const msgs = messages.map(message => {
          return { text: message.content, isBot: message.speaker === "user" ? false : true }
        });
        // console.log(msgs)
        setMessages([...msgs])
      })
      .catch((e) => {
        console.error(e);
      });
  }, []);

  axiosRetry(axios, {
    retries: 10, // number of retries
    retryDelay: (retryCount) => {
      console.log(`retry attempt: ${retryCount}`);
      return retryCount * 10000; // time interval between retries
    },
    retryCondition: (error) => {
      // if retry condition is not specified, by default idempotent requests are retried
      return error.response.status === 404;
    },
  });


  const handleSend = async () => {
    const text = input;
    if (!text) return
    setInput('');
    setMessages([
      ...messages,
      { text: text, isBot: false },
    ]);
    const res = await axios.post(`http://${IP_ADDRESS}/api/message/`, {
      "content": text,
      "dialogID": dialogID,
      "role": "user",
      "userID": 1
    }).catch((err) => {
      if (err.response.status !== 200) {
        throw new Error(`API call failed with status code: ${err.response.status} after 3 retry attempts`);
      }
    });
    console.log(res)
    const message = res.data.message
    const response = await axios.get(`http://${IP_ADDRESS}/api/message/response/`, {
      params: {
        userID: message.userID,
        dialogID: message.dialogID,
        messageID: message.messageID,
      }
    }).catch((err) => {
      if (err.response.status !== 200) {
        throw new Error(`API call failed with status code: ${err.response.status} after 3 retry attempts`);
      }
    });
    console.log(response)
    const response_message = response.data
    setMessages([
      ...messages,
      { text: text, isBot: false },
      { text: response_message.content, isBot: true },
    ]);
  }

  const handlerEnter = async (e) => {
    if (e.key === 'Enter') await handleSend();
  }

  const switchDialog = async (e) => {
    const dialogIndex = e.target.value;
    axios.get(`http://${IP_ADDRESS}/api/message/allDialogs/`, {
      params: { "user_id": 1 }
    })
      .then((res) => {
        if (dialogs[dialogIndex].messages.length === 0) {
          setMessages([])
        } else {
          const response_dialogs = res.data.dialogs
          // console.log(response_dialogs)
          setDialogs(response_dialogs)
          // console.log(dialogs)
          const messages = response_dialogs[dialogIndex].messages
          // console.log(messages)
          const msgs = messages.map(message => {
            return { text: message.content, isBot: message.speaker === "user" ? false : true }
          });
          // console.log(msgs)
          setMessages([...msgs])
        }
      })
      .catch((e) => {
        console.error(e);
      });
  }

  const createNewDialog = async () => {
    const newdialogID = dialogs.length + 1
    setDialogID(newdialogID)
    setDialogs([...dialogs, { "dialogID": newdialogID, "messages": [] }])
  }


  return (
    <div className="App">
      <div className='sideBar'>
        <div className='upperSide'>
          <div className='upperSideTop'>
            <img src={GPTLogo} alt='Logo' className='logo' /><span className='brand'>ChatGPT</span>
          </div>
          {/* <button className='midBtn' onClick={() => { window.location.reload() }}><img src={addBtn} alt='' className='addBtn' />New Chat</button> */}
          <div className='upperSideBottom'>
            {/* <button className='query' onClick={handleQuery} value={'What is Programming?'}><img src={msgIcon} alt='Query' />What is Programming ?</button>
            <button className='query' onClick={handleQuery} value={'How to use an API?'}><img src={msgIcon} alt='Query' />How to use an API ?</button> */}
            {dialogs.map((dialog, i) =>
              <div key={i} >
                <button className='query' onClick={switchDialog} value={i}><img src={msgIcon} alt='Query' />dialog {i + 1}</button>
              </div>
            )}
            <div >
              <button className='query' onClick={createNewDialog}><img src={msgIcon} alt='Query' />New dialog </button>
            </div>
          </div>
        </div>
        <div className='lowerSide'>
          <div className='listItems'><img src={home} alt='' className='listItemsImg' />Home</div>
          <div className='listItems'><img src={saved} alt='' className='listItemsImg' />Saved</div>
          <div className='listItems'><img src={rocket} alt='' className='listItemsImg' />Upgrade to Pro</div>
        </div>
      </div>
      <div className='main'>
        <div className='chats'>
          {messages.map((message, i) =>
            <div key={i} className={message.isBot ? 'chat bot' : 'chat'}>
              <img className='chatImg' src={message.isBot ? GPTImgLogo : userIcon} alt='' />{message.text}<p className='txt'></p>
            </div>
          )}
          <div ref={msgEnd} />
        </div>
        <div className='chatFooter'>
          <div className='inp'>
            <input type='text' placeholder='Send a message' value={input} onKeyDown={handlerEnter} onChange={(e) => { setInput(e.target.value) }} />
            <button className='send'><img src={sendBtn} alt='Send' onClick={handleSend} /></button>
          </div>
          <p>ChatGPT may produce inaccurate information about people , plcaes , or facts. ChatGPT August 20 Version.</p>
        </div>
      </div>
    </div>
  );
}

export default App;
